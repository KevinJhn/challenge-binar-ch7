const router = require("express").Router();
const auth = require("../controllers/authController");

router.get("/", (req, res) => res.render("home", {
    username: "baba"
}))

router.post("/register", auth.register)
router.post("/login", auth.login)


module.exports = router